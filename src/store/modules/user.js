/* eslint-disable no-unused-vars */
import api from '@/api'

import { getToken } from '@/utils/auth'

const state = {
  modules: {},
  token: getToken(),

  partner: {},
  image_url: undefined,
  uid: undefined,
  partner_id: undefined,
  name: undefined,
  session_info: {},
  so_reg_company: {}

  // 'user_companies': {'current_company': (2, 'Comp1'),
  // 'allowed_companies': [(2, 'Comp1'), (1, 'My Company')]}
}

const mutations = {
  SET_so_reg_company: (state, paylaod = {}) => {
    state.so_reg_company = { ...paylaod }
  },
  SET_session_info: (state, session_info = {}) => {
    state.session_info = { ...session_info }
  },

  SET_TOKEN: (state, token) => {
    state.token = token
  },

  SET_ME: (state, { partner, uid, partner_id }) => {
    state.partner = { ...partner }

    state.image_url = partner.image_128__url

    state.isEmployee = partner.employee
    state.uid = uid
    state.partner_id = partner_id
    state.name = partner.name
  },

  SET_IMAGE: (state, image_url) => {
    state.image_url = image_url
  },

  SET_UID: (state, { uid }) => {
    state.uid = uid
  }
}

const actions_reg_company = {
  async check_company_name({ commit, state, dispatch }, payload) {
    const model = api.env('res.users')
    const result = await model.check_company_name(payload)
    return new Promise((resolve, reject) => resolve(result))
  },

  async chech_reg_company({ commit, state, dispatch }, payload) {
    const model = api.env('res.users')
    const so = await model.chech_reg_company()
    // console.log(so)
    commit('SET_so_reg_company', so)
    return new Promise((resolve, reject) => resolve())
  },

  async create_company({ commit, state, dispatch }, payload) {
    const model = api.env('res.users')
    const res1 = await model.create_company(payload)
    await dispatch('getSessionInfo')
    return dispatch('chech_reg_company')
  },

  async reg_company_pay({ commit, state, dispatch }, payload) {
    const model = api.env('res.users')
    const res1 = await model.reg_company_pay(payload)
    // await dispatch('getSessionInfo')
    return dispatch('chech_reg_company')
  },

  async reg_company({ commit, state, dispatch }, payload) {
    const model = api.env('res.users')
    const res1 = await model.reg_company(payload)
    // await dispatch('getSessionInfo')
    return dispatch('chech_reg_company')
  },

  async close_reg_company({ commit, state, dispatch }, payload) {
    const model = api.env('res.users')
    const res1 = await model.close_reg_company(payload)
    // await dispatch('getSessionInfo')
    return dispatch('chech_reg_company')
  },
  async set_company({ commit, state, dispatch }, payload) {
    const model = api.env('res.users')
    const res1 = await model.write(state.uid, { company_id: payload })
    return dispatch('getSessionInfo')
  }
}

const actions_auth = {
  async auth_codesend({ commit }, payload) {
    // const { username, password } = userInfo
    const res = await api.rpc.auth_codesend(payload)
    return new Promise((resolve, reject) => resolve(res))
  },

  async auth_codeback({ commit }, payload) {
    const code = await api.rpc.auth_codeback(payload)
    return new Promise((resolve, reject) => resolve(code))
  },

  async auth_exist({ commit }, payload) {
    const code = await api.rpc.auth_exist(payload)
    return new Promise((resolve, reject) => resolve(code))
  },

  async auth_login({ commit, dispatch }, payload) {
    const response = await api.rpc.auth_login(payload)
    return dispatch('afterLogin', response)
  },

  async auth_resetpsw({ commit, dispatch }, payload) {
    // const { email, code, password } = payload
    // console.log('xxxx, resetPassword, ', email, code, password)

    const response = api.rpc.auth_resetpsw(payload)

    return new Promise((resolve, reject) => {
      response
        .then(res => {
          dispatch('resetToken')
          resolve(res)
        })
        .catch(error => {
          reject(error)
        })
    })
  },

  async auth_register({ commit, dispatch }, payload) {
    // const { mobile, code, username, password } = payload
    const response = api.rpc.auth_register(payload)
    return new Promise((resolve, reject) => {
      response
        .then(res => {
          dispatch('resetToken')
          resolve(res)
        })
        .catch(error => {
          reject(error)
        })
    })
  }
}

const actions = {
  ...actions_reg_company,

  ...actions_auth,

  async getSessionInfo({ commit, state, dispatch }) {
    //
    const session = await api.rpc.get_session_info()
    // console.log('me getSessionInfo', session)

    commit('SET_UID', { uid: session.uid })
    commit('SET_session_info', session)

    return new Promise((resolve, reject) => {
      resolve(session)
    })
  },

  async session_check({ commit, state, dispatch }) {
    await api.rpc.session_check()
    const session = await api.get_userinfo()
    // console.log('me getInfo', session)
    commit('SET_UID', { uid: session.uid })
    commit('SET_session_info', session)

    return new Promise((resolve, reject) => {
      resolve()
    })
  },

  async getInfo({ commit, state, dispatch }) {
    // const session = await api.get_userinfo()
    // console.log('me getInfo', session)
    // const { uid, partner_id } = await api.get_userinfo()
    // console.log('me getInfo', uid, partner_id)
    // const Ptn = api.env('res.partner')
    // const partner = await Ptn.browse_one(partner_id, {
    //   fields: { name: null, employee: null, title: null }
    // })
    // console.log('xxxx, getInfo', partner)
    // commit('SET_ME', { partner, uid, partner_id })
    // return new Promise(resolve => {
    //   // commit('SET_CURRENT', current)
    //   resolve({ uid, partner_id, partner })
    // })
  },

  // user login
  async login({ commit, dispatch }, userInfo) {
    const { username, password } = userInfo
    const response = await api.rpc.login({
      username: username.trim(),
      password: password
    })
    // console.log('xxxxx, login,', dispatch)
    return dispatch('afterLogin', response)
  },

  // user logout
  logout({ commit, state, dispatch }) {
    return new Promise((resolve, reject) => {
      api.rpc
        .logout()
        .then(() => {
          dispatch('resetToken')
          resolve()
        })
        .catch(error => {
          reject(error)
        })
    })
  },

  async afterLogin({ commit }, response) {
    // console.log('xxxx, afterLogin,,', response)
    const session = await api.get_userinfo()
    // console.log('me getInfo', session)
    commit('SET_UID', { uid: session.uid })
    commit('SET_session_info', session)

    return new Promise((resolve, reject) => {
      const { token } = response
      if (token) {
        commit('SET_TOKEN', token)
        resolve(token)
      } else {
        // console.log('xxxx, afterLogin,reject,', response)
        reject(response)
      }
    })
  },

  // remove
  async resetToken({ commit }) {
    // console.log('resetToken')
    await api.rpc.reset_token()
    return new Promise(resolve => {
      commit('SET_TOKEN', '')
      resolve()
    })
  },

  async changePassword({ commit, dispatch }, payload) {
    const { oldPsw, newPsw } = payload
    const User = api.env('res.users')

    const response = User.change_password(oldPsw, newPsw)

    return new Promise((resolve, reject) => {
      response
        .then(res => {
          dispatch('resetToken')
          resolve(res)
        })
        .catch(error => {
          reject(error)
        })
    })
  },

  async change_image({ state, commit, dispatch }, payload) {
    const { image } = payload

    const ss = image.split(';base64,')
    const raw_image = ss[1]

    const User = api.env('res.users')

    const { uid } = state

    const response = User.write({
      id: uid,
      image_1920: raw_image
    })

    return new Promise((resolve, reject) => {
      response
        .then(res => {
          const { partner } = state

          const date = new Date().getTime()
          const image_url2 = `${partner.image_128__url}&unique=${date}`
          // console.log('change_image,', image_url2)

          commit('SET_IMAGE', image_url2)

          resolve(res)
        })
        .catch(error => {
          reject(error)
        })
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

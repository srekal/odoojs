const formData_to_dataDict = (values, fieldsGet) => {
  return Object.keys(values).reduce((acc, cur) => {
    const meta = fieldsGet[cur] || {}
    const isSelect = ['many2one', 'selection'].includes(meta.type)
    if (values[cur] && isSelect) {
      const ops = meta.options.filter(item => item[0] === values[cur])
      acc[cur] = values[cur]
      acc[`${cur}__name`] = ops.length ? ops[0][1] : values[cur]
    } else {
      acc[cur] = values[cur]
    }
    return acc
  }, {})

  // return Object.keys(values).reduce((acc, cur) => {
  //   const meta = fieldsGet[cur] || {}
  //   const isSelect = ['many2one', 'selection'].includes(meta.type)
  //   if (values[cur] && isSelect) {
  //     const ops = meta.options.filter(item => item[0] === values[cur])
  //     acc[cur] = ops.length ? ops[0] : values[cur]
  //   } else {
  //     acc[cur] = values[cur]
  //   }
  //   return acc
  // }, {})
}

const dataDict_to_formData = values => {
  // console.log('dataDict_to_formData1, ', values)
  return Object.keys(values)
    .filter(item => !(item.split('__').length > 1 && item.split('__')[0]))
    .reduce((acc, cur) => {
      acc[cur] = values[cur]
      return acc
    }, {})
}

// function toPrice(num) {
//   num = parseFloat(num)
//     .toFixed(2)
//     .toString()
//     .split('.')
//   num[0] = num[0].replace(new RegExp('(\\d)(?=(\\d{3})+$)', 'ig'), '$1,')
//   return '￥' + num.join('.')
// }

// const formateList = (records, meta) => {
//   // return records.map(item => formateDict2(item, meta))
// }

// const formateDict1 = (rec, meta) => {
//   return Object.keys(rec).reduce((acc, cur) => {
//     if (meta[cur] && ['select'].includes(meta[cur].type)) {
//       if (meta[cur].multiple) {
//         // TBD
//         acc[cur] = rec[cur] && rec[cur][0]
//       } else {
//         acc[cur] = rec[cur] && rec[cur][0]
//       }
//     } else {
//       acc[cur] = rec[cur]
//     }

//     return acc
//   }, {})
// }

// const formateDict2 = (rec, meta) => {
//   return Object.keys(rec).reduce((acc, cur) => {
//     if (meta[cur] && ['select'].includes(meta[cur].type)) {
//       if (meta[cur].multiple) {
//         // TBD
//         acc[cur] = rec[cur] && rec[cur][1]
//       } else {
//         acc[cur] = rec[cur] && rec[cur][1]
//       }
//     } else if (meta[cur] && meta[cur].type === 'monetary') {
//       acc[cur] = toPrice(rec[cur])
//     } else {
//       acc[cur] = rec[cur]
//     }

//     return acc
//   }, {})
// }

const vuex_store_creator = api => {
  const state = {
    // metadata: {},
    // model: '',
    // fieldsGet: {},
    // dataCount: 0,
    // dataList: [],
    // listO2m: [],
    // dataId: 0,
    // values: {},
    // formData: {},
    // valuesForParent: {},
    // dataDict: {},
  }

  const mutations = {
    SET_FieldsGet: (state, payload) => {
      state.fieldsGet = { ...state.fieldsGet, ...payload }
    },

    SET_DataCount: (state, dataCount) => {
      state.dataCount = dataCount
    },

    SET_DataList: (state, dataList) => {
      state.dataList = [...dataList]
    },

    SET_DataList_ForReport: (state, dataList) => {
      state.dataList = [...dataList]
    },

    SET_ListO2m: (state, payload) => {
      state.listO2m = [...payload]
    },

    SET_DataId: (state, payload) => {
      state.dataId = payload
    },
    SET_Values: (state, payload) => {
      state.values = { ...payload }
    },

    SET_FormData: (state, payload) => {
      state.formData = { ...payload }
    },

    SET_ValuesForParent: (state, payload) => {
      state.valuesForParent = { ...payload }
    },

    SET_DataDict_ForReport: (state, row) => {
      state.dataDict = { ...row }
    },

    SET_DataDict: (state, row) => {
      state.dataDict = { ...row }
    }
  }

  const actions_api_crud = {
    async web_search_read({ state, commit }, { isEdit, ...payload }) {
      // console.log('in web_search_read:', payload, state.model)
      // commit('productTemplate/SET_Options', { a: 123 }, { root: true })
      // const action = 'productTemplate/search_count'
      // dispatch(action, { a: 1 }, { root: true })

      const result = await api.env(state.model).web_search_read(payload)
      // console.log('in web_search_read, ok', result)

      commit('SET_DataList', result.records)
      commit('SET_DataCount', result.length)

      if (isEdit) {
        const lines = result.records.map(item => {
          const formData = dataDict_to_formData(item)
          return {
            op: 4,
            id: item.id,
            values: {},
            formData,
            // formData: formateDict1(item, state.metadata.columnsForPage),
            dataDict: { ...item }
          }
        })
        commit('SET_ListO2m', lines)
      }

      return new Promise(resolve => resolve())
    },

    async search_count({ state, commit }, payload) {
      // console.log('in search_count 1:', payload)
      // console.log('in search_count 2:', state.model)
      const result = await api.env(state.model).search_count(payload)
      commit('SET_DataCount', result)
      return new Promise(resolve => resolve())
    },

    async search_one({ state, commit, dispatch }, payload) {
      const callback = records => {
        // console.log('cb', records)
      }
      const result = await api
        .env(state.model)
        .search_one({ ...payload, callback })
      return dispatch('_after_browse_one', { ...payload, result })
    },

    async browse_one({ state, commit, dispatch }, payload2) {
      const { isEdit, id: rid, ...payload } = payload2
      const result = await api.env(state.model).browse_one(rid, payload)
      return dispatch('_after_browse_one', { ...payload2, result, isEdit })
      // commit('SET_DataDict', result)
      // return new Promise(resolve => resolve())
    },

    async _after_browse_one({ state, commit, dispatch }, payload) {
      // console.log('----browse_one, ', )
      const { isEdit, result, context } = payload
      const { columnsForPage: cols = {} } = state.metadata
      const fields = Object.keys(result)
      // console.log('----browse_one, ', state.model)

      for (let i = 0; i < fields.length; i++) {
        const fld = fields[i]
        const val = result[fld]
        const meta = cols[fld] || {}
        // console.log('table----xxxx, ', fld, meta)
        if (meta.type === 'table' && meta.odooModel) {
          // console.log('xxxxx,browse_one, table, ', fld, meta)
          //  TBD 编辑页面, 子表 刷新数据后, 父表的  字段  应该 改为
          //  [ 4, id, ] 这样子
          result[fld] = val.map(item => [4, item, null])
          // 因此这里的 应该是 设计一个  o2m_web_search_read action 单独处理这件事
          // 而不是现在这样, 直接 call web_search_read
          // 刷新子表后, 返回来 应该 设置 父表
          const o2m_model = api.model_odoo2vuex(meta.odooModel)
          const action = `${o2m_model}/web_search_read`
          dispatch(
            action,
            { domain: { id: val }, isEdit, context },
            { root: true }
          )
        }
      }

      // console.log('browse one ', result)

      commit('SET_DataDict', result)

      if (isEdit) {
        const result2 = api
          .env(state.model)
          .dataDict_to_formData(result, state.fieldsGet)

        commit('SET_FormData', result2)
        commit('SET_Values', {})
        commit('SET_DataId', result.id)
      }
      return new Promise(resolve => resolve())
    },

    async write({ state, commit }, payload) {
      const rid = state.formData.id
      const result = await api
        .env(state.model)
        .write(rid, state.values, payload)
      commit('SET_DataDict', result)
      return new Promise(resolve => resolve())
    },

    async create({ state, commit }, payload) {
      // console.log('xxxxx, in create, value', state.values)
      const result = await api.env(state.model).create(state.values, payload)
      commit('SET_DataDict', result)
      return new Promise(resolve => resolve())
    },

    async unlink({ state, commit }, { id: rid }) {
      const result = await api.env(state.model).unlink(rid)
      if (result) {
        commit('SET_DataDict', {})
      }
      return new Promise(resolve => resolve(result))
    }
  }

  const actions_api_workflow = {
    async workflow2({ state, commit }, { name, line_field, line_values }) {
      const rid = state.dataDict.id
      // console.log('xxx, workflow, ,', name, line_field, line_values)
      const result = await api.env(state.model).workflow2(rid, name, {
        values: state.dataDict,
        line_field,
        line_values
      })
      const { record } = result || {}
      if (record) {
        commit('SET_DataDict', record)
      }
      return new Promise(resolve => resolve(result))
    },
    async workflow({ state, commit }, { name }) {
      //
      const rid = state.dataDict.id
      // console.log('xxx, workflow, ,', name, rid, meta)
      const result = await api
        .env(state.model)
        .workflow(rid, name, state.dataDict)
      const { record } = result || {}

      // 需要判断  是否 更新 SET_DataDict
      if (record) {
        commit('SET_DataDict', record)
      }
      return new Promise(resolve => resolve(result))
    }
  }

  const actions_api_new = {
    async _after_fields_get({ state, commit }, { result }) {
      // eslint-disable-next-line no-unused-vars
      const sss = () => {
        // const flds = Object.keys(result)
        // for (let i = 0; i < flds.length; i++) {
        //   const item = flds[i]
        //   // const meta = result[item]
        //   const meta = state.fieldsGet[item]
        //   if (meta.type === 'many2one') {
        //     console.log('xx,', item, meta)
        //     const res = await api
        //       .env(meta.relation)
        //       .name_search({ domain: meta.domain })
        //     console.log('xx,1,', res)
        //     const me = { ...result[item] }
        //     me.options = res
        //     // const p2 = {}
        //     // p2[item] = me
        //     result[item] = me
        //     // commit('SET_FieldsGet', p2)
        //   }
        // }
      }

      Object.keys(result).forEach(item => {
        const meta = state.fieldsGet[item]
        const isToCall =
          meta.type === 'many2one' && !meta._set && Array.isArray(meta.domain)
        if (isToCall) {
          // console.log('xx,_after_fields_get,', item, meta)
          const columnsForPage = state.metadata.columnsForPage || {}
          const colOfPage = columnsForPage[item] || {}
          const domain1 = colOfPage.domain
          // console.log('xx,_after_fields_get,', item, meta, domain1)

          const domain =
            domain1 && typeof domain1 === 'function'
              ? domain1({ self: api.env(state.model), record: state.dataDict })
              : domain1 || {}

          const rel = api.env(meta.relation)
          const res2 = rel.name_search({ domain, domain2: meta.domain })
          res2.then(res => {
            const meta2 = state.fieldsGet[item]
            if (!meta2._set) {
              const nfgs = { [item]: { ...meta2, options: res, _set: 'init' } }
              commit('SET_FieldsGet', nfgs)
            }
          })
        }
      })
      return new Promise(resolve => resolve())
    },

    async fields_get({ state, commit, dispatch }, payload2) {
      // fields_get 什么时候 被调用?
      // 1 onchange 自己带 fields_get
      // 2 read 函数 服务端已经处理过  m2o 字段了
      // 3 编辑页面 需要 自己先处理一次 fields_get
      // 3 只有 过滤条件 一个地方 需要用 fields_get 了?

      // console.log('----xxx----, fields_get,', state.model, payload2)
      const { globals_dict, ...payload } = payload2 || {}
      // TBD 从 父表 取 form data 但是 父表什么时候 更新完成数据?
      const valuesForParent = state.valuesForParent
      const formData = state.formData
      const values = { ...valuesForParent, ...formData }

      // 对于 父表编辑页面, formData 还没有获得
      // 不过暂时 get_globals_dict 只是设置 company_id, 不需要 formData

      const globals_dict2 = api
        .env(state.model)
        .get_globals_dict({ values, globals_dict, ...payload })

      const result = await api
        .env(state.model)
        .fields_get({ ...payload, globals_dict: globals_dict2 })
      // console.log('xxx, fg,', result)

      commit('SET_FieldsGet', result)
      await dispatch('_after_fields_get', { result })
      return new Promise(resolve => resolve())
    },

    async _after_onchange({ state, commit, dispatch }, { result = {} }) {
      //
      // console.log('xxx, _after_onchange', result)
      const {
        domain: onchange_domains,
        value = {},
        value_form,
        fields,
        isDefault
      } = result

      const commit_fieldsGet = () => {
        const fgs = { ...state.fieldsGet, ...fields }
        const fieldsGet = Object.keys(fgs).reduce((acc, cur) => {
          const item = {
            ...(state.fieldsGet[cur] || {}),
            ...(fields[cur] || {})
          }
          if (item.type === 'many2one' && !item._set && value[cur]) {
            item.options = [value[cur]]
          }
          acc[cur] = item
          return acc
        }, {})

        commit('SET_FieldsGet', fieldsGet)
      }

      const commit_value = () => {
        commit('SET_DataDict', { ...state.dataDict, ...value })
        commit('SET_FormData', { ...state.formData, ...value_form })
        if (isDefault) {
          commit('SET_Values', { ...state.values, ...value_form })
        }
      }

      const dispatch_domain = () => {
        Object.keys(onchange_domains).forEach(item => {
          //
          const meta = state.fieldsGet[item] || {}
          const domain = [...meta.domain, ...onchange_domains[item]]
          api
            .env(meta.relation)
            .name_search({ domain })
            .then(res => {
              commit('SET_FieldsGet', {
                [item]: { ...state.fieldsGet[item], options: res, _set: 'init' }
              })
            })
        })
      }

      // 1 更新 fieldsGet
      // 2 更新 fieldsGet 的 options, 仅仅用获取的值来更新
      commit_fieldsGet()
      // 3 更新 dataDict, formData, values
      commit_value()
      // 4 更新 fieldsGet 的 options, 根据 domain 来更新
      dispatch_domain()
      // 5 因为第1步可能带回来新的 m2o, 或者domian 变化过的, 因此 再刷新一遍 options
      dispatch('_after_fields_get', { result: fields })
      return new Promise(resolve => resolve())
    },

    async default_get_onchange({ state, commit, dispatch }, payload = {}) {
      // console.log('xxxxx, in default_get_onchange', payload)
      // 新增时 调用 default_get_onchange
      // 调用之前, 必须先调一次 fields_get
      // 若不调用 fields_get, 该 default_get_onchange 也会返回 fields?

      commit('SET_DataDict', {})
      commit('SET_FormData', {})
      commit('SET_Values', {})

      // TBD 从 父表 取 form data 但是 父表什么时候 更新完成数据?
      const values = { ...state.valuesForParent }

      const result = await api
        .env(state.model)
        .default_get_onchange({ ...payload, ids: [], values, field_name: [] })

      const { columnsForPage: cols = {} } = state.metadata
      Object.keys(cols).forEach(item => {
        const meta = cols[item] || {}
        if (meta.type === 'table' && meta.odooModel) {
          const o2m_model = api.model_odoo2vuex(meta.odooModel)
          const mutation = `${o2m_model}/SET_DataList`
          commit(mutation, [], { root: true })
        }
      })

      // console.log('xxx, default_get_onchange', result)
      await dispatch('_after_onchange', { result: { ...result, isDefault: 1 } })
      // console.log(
      //   'xxx, default_get_onchange 2',
      //   state.formData.date_order,
      //   JSON.stringify(state.formData.date_order)
      // )
      // console.log('xxx, default_get_onchange 3', JSON.stringify(state.formData))
      return new Promise(resolve => resolve())
    },

    async onchange({ state, commit, dispatch }, { field, value }) {
      // console.log('xxxxx, in onchange, value', state.model, field, value)
      // 打开编辑页面, 必须调用过  fields_get, 知道所有编辑字段是否是 m2o / selection

      const values = { [field]: value }
      commit('SET_Values', { ...state.values, ...values })
      commit('SET_FormData', { ...state.formData, ...values })
      const dataDict = formData_to_dataDict(values, state.fieldsGet)
      commit('SET_DataDict', { ...state.dataDict, ...dataDict })

      // console.log('xxxxx, in onchange, valuesForParent', state.valuesForParent)
      // console.log('xxxxx, in onchange, formData', state.formData)

      const values2 = { ...state.formData, ...state.valuesForParent }
      delete values2.id
      const ids = state.dataDict.id > 0 ? [state.dataDict.id] : []
      // console.log('xxxxx, in onchange, values2', values2)
      const result = await api
        .env(state.model)
        .onchange({ ids, values: values2, field_name: field })

      // console.log('xxx, onchange', result)
      await dispatch('_after_onchange', { result })
    }
  }

  const actions_api_o2m = {
    async set_o2mForm({ state, dispatch }, { field }) {
      const { columnsForPage = {} } = state.metadata
      const { odooModel } = columnsForPage[field] || {}
      const o2m_model = api.model_odoo2vuex(odooModel)
      await dispatch('set_o2mParent', { field })
      await dispatch(`${o2m_model}/fields_get`, {}, { root: true })
      return new Promise(resolve => resolve())
    },

    async set_o2mParent({ state, commit }, { field }) {
      const { columnsForPage = {} } = state.metadata
      const { odooModel, refName } = columnsForPage[field] || {}
      const o2m_model = api.model_odoo2vuex(odooModel)
      commit(
        `${o2m_model}/SET_ValuesForParent`,
        { [refName]: { ...state.formData } },
        { root: true }
      )
      return new Promise(resolve => resolve())
    },

    async o2m_default_get_onchange(context2, { field }) {
      const { state, commit, dispatch, rootState } = context2
      const { columnsForPage = {} } = state.metadata
      const { odooModel } = columnsForPage[field] || {}
      const o2m_model = api.model_odoo2vuex(odooModel)
      const o2m_state = rootState[o2m_model]

      // console.log(' o2m_default_get_onchange', field)
      // console.log(' o2m_default_get_onchange, state.formData', state.formData)

      await dispatch('set_o2mParent', { field })

      await dispatch(`${o2m_model}/default_get_onchange`, {}, { root: true })

      const get_new_o2m_id = () => {
        // console.log('xxxxx,get_new_o2m_id,  ', o2m_state.listO2m)
        const ids = o2m_state.listO2m.map(line => line.id)
        // console.log('xxxxx,get_new_o2m_id, ids,  ', ids)
        const min_id = ids.length ? Math.min(...ids) : 0

        // const new_id = (min_id < 0 ? min_id : 0) - 1
        // console.log('xxxxx,get_new_o2m_id, min_id,', min_id)
        // console.log('xxxxx,get_new_o2m_id, new_id ', new_id)
        return (min_id < 0 ? min_id : 0) - 1
      }

      const o2m_id = get_new_o2m_id()
      commit(`${o2m_model}/SET_DataId`, o2m_id, { root: true })
      commit(
        `${o2m_model}/SET_DataDict`,
        { id: o2m_id, ...o2m_state.dataDict },
        { root: true }
      )
      commit(
        `${o2m_model}/SET_FormData`,
        { id: o2m_id, ...o2m_state.formData },
        { root: true }
      )

      return new Promise(resolve => resolve())
    },

    async o2m_edit({ state, commit, rootState, dispatch }, { field, o2m_id }) {
      // console.log('xxx, o2m_edit', field, o2m_id, typeof o2m_id)
      const { columnsForPage = {} } = state.metadata
      const { odooModel } = columnsForPage[field] || {}
      const o2m_model = api.model_odoo2vuex(odooModel)
      const o2m_state = rootState[o2m_model]

      await dispatch('set_o2mParent', { field })

      const lines = o2m_state.listO2m.filter(item => item.id === o2m_id)
      // console.log('xxx, o2m_edit 2', o2m_state.listO2m, lines)

      if (!lines.length) {
        return new Promise(resolve => resolve())
      }
      const line = lines[0]

      if (![0, 1, 4].includes(line.op)) {
        return new Promise(resolve => resolve())
      }

      commit(`${o2m_model}/SET_DataId`, o2m_id, { root: true })
      commit(`${o2m_model}/SET_DataDict`, { ...line.dataDict }, { root: true })
      commit(`${o2m_model}/SET_FormData`, { ...line.formData }, { root: true })
      commit(`${o2m_model}/SET_Values`, { ...line.values }, { root: true })

      if (line.fieldsGet) {
        const fgs = Object.keys(line.fieldsGet).reduce((acc, cur) => {
          acc[cur] = { ...line.fieldsGet[cur] }
          return acc
        }, {})
        commit(`${o2m_model}/SET_FieldsGet`, fgs, { root: true })
      } else {
        const action = `${o2m_model}/fields_get`
        await dispatch(action, { page: 'o2mEdit' }, { root: true })
      }

      return new Promise(resolve => resolve())
    },

    async o2m_onchange({ state, dispatch }, payload) {
      // console.log(' o2m edit, o2m_onchange 2', payload)

      const { field, o2m_field, o2m_value } = payload

      const { columnsForPage = {} } = state.metadata
      const { odooModel } = columnsForPage[field] || {}
      const o2m_model = api.model_odoo2vuex(odooModel)
      // const o2m_state = rootState[o2m_model]
      const payload2 = { field: o2m_field, value: o2m_value }
      // await dispatch('_o2m_onchange_after', { field })
      await dispatch(`${o2m_model}/onchange`, payload2, { root: true })
      await dispatch('_o2m_onchange_after', { field })

      return new Promise(resolve => resolve())
    },

    async _o2m_onchange_after({ state, dispatch, rootState }, { field }) {
      const { columnsForPage = {} } = state.metadata
      const { odooModel } = columnsForPage[field] || {}
      const o2m_model = api.model_odoo2vuex(odooModel)
      const o2m_state = rootState[o2m_model]
      const o2m_id = o2m_state.dataId
      const dataDict = { ...o2m_state.dataDict }
      const formData = { ...o2m_state.formData }
      const values = { ...o2m_state.values }

      const fieldsGet = Object.keys(o2m_state.fieldsGet).reduce((acc, cur) => {
        acc[cur] = { ...o2m_state.fieldsGet[cur] }
        return acc
      }, {})

      const newline = { id: o2m_id, dataDict, formData, values, fieldsGet }
      // console.log(' o2m edit, o2m_onchange newline', newline)

      const get_listO2m = () => {
        const lines = o2m_state.listO2m.filter(line => line.id === o2m_id)

        if (lines.length) {
          return o2m_state.listO2m.map(line => {
            if (line.id !== o2m_id) {
              return line
            } else {
              return { op: line.op === 4 ? 1 : line.op, ...newline }
            }
          })
        } else {
          return [...o2m_state.listO2m, { op: 0, ...newline }]
        }
      }

      const listO2m = get_listO2m()
      // console.log(' o2m edit, o2m_onchange listO2m', listO2m)
      await dispatch(`${o2m_model}/save_listO2m`, { listO2m }, { root: true })
      return new Promise(resolve => resolve())
    },

    async o2m_submit({ state, dispatch, rootState }, { field }) {
      // 这一步 不应该 写 save_listO2m, 应该 只取 values , 去触发 父表 的 onchange
      await dispatch('_o2m_onchange_after', { field })
      await dispatch('_o2m_submit', { field })
      return new Promise(resolve => resolve())
    },

    async save_listO2m({ commit }, { listO2m }) {
      commit('SET_ListO2m', listO2m)
      const dataList2 = listO2m.filter(line => line.op !== 2)
      const dataList = dataList2.map(line => line.dataDict)
      commit('SET_DataList', dataList)
      return new Promise(resolve => resolve())
    },

    async o2m_del({ state, dispatch, rootState }, { field, o2m_id }) {
      // console.log('xxxx, o2m_del', field, o2m_id)
      const { columnsForPage = {} } = state.metadata
      const { odooModel } = columnsForPage[field] || {}
      const o2m_model = api.model_odoo2vuex(odooModel)
      const o2m_state = rootState[o2m_model]

      const listO2m = o2m_state.listO2m.reduce((acc, line) => {
        if (line.id !== o2m_id) {
          acc.push(line)
        } else if (line.op !== 0) {
          acc.push({ op: 2, id: line.id })
        }
        return acc
      }, [])
      // console.log('xxxx, o2m_del, 2,', field, listO2m)
      await dispatch(`${o2m_model}/save_listO2m`, { listO2m }, { root: true })
      await dispatch('_o2m_submit', { field })

      return new Promise(resolve => resolve())
    },

    async _o2m_submit({ state, dispatch, rootState }, { field }) {
      const { columnsForPage = {} } = state.metadata
      const { odooModel } = columnsForPage[field] || {}
      const o2m_model = api.model_odoo2vuex(odooModel)
      const lines = rootState[o2m_model].listO2m.map(ln => {
        return [ln.op, ln.id, [1, 0].includes(ln.op) ? ln.values : null]
      })
      const payload2 = { field, value: lines }
      // console.log('xxxx, _o2m_submit,', field, payload2)

      await dispatch('onchange', payload2)
      return new Promise(resolve => resolve())
    }
  }

  const actions_api_report = {
    async report_month({ state, commit }, payload) {
      // console.log('xxx,wizard_submit 1 ,', payload)
      const result = await api.env(state.model).report_month(payload)
      // console.log('xxx,wizard_submit ok ,', result, state)
      commit('SET_DataDict_ForReport', result)
      return new Promise(resolve => resolve(result))
    },

    async search_report_month_list({ state, commit }, payload) {
      // console.log('xxx,wizard_submit 1 ,', payload)
      const result = await api
        .env(state.model)
        .search_report_month_list(payload)
      // console.log('xxx,wizard_submit ok ,', result, state)
      commit('SET_DataList_ForReport', result)
      return new Promise(resolve => resolve(result))
    }
  }

  const actions = {
    ...actions_api_crud,
    ...actions_api_workflow,
    ...actions_api_new,
    ...actions_api_o2m,
    ...actions_api_report
  }

  return {
    state: {
      dataCount: 0,
      dataList: [],
      dataDict: {},
      fieldsGet: {},
      formData: {},
      values: {},
      valuesForParent: {},
      listO2m: [],
      dataId: 0,
      ...state
    },
    mutations,
    actions
  }
}

export default vuex_store_creator

const AccountMoveLineForInvoice = {
  // invoice 的 分录行
  _inherit: 'account.move.line',

  metadata: {
    columnsForList: {
      account_id: null,
      name: null,
      debit: null,
      credit: null
    },

    forAppList: {
      title: rec => {
        return `${rec.account_id__name}`
      },
      // icon: rec => {
      //   // 编辑的 新增的时候 没有 product_id__object 字段
      //   return `${(rec.product_id__object || {}).image_128__url}`
      // },

      label: rec => {
        return `${rec.name || ''}`
      },

      value: rec => {
        return `借:${rec.debit}  贷: ${rec.credit}`
      }
    }
  }
}

const AccountMoveLineInvoice13 = {
  _inherit: '_account.move.line.invoice',

  metadata: {
    context: {
      default_type: 'out_invoice'
    }
  }
}

const AccountMoveLineInvoice14 = {
  _inherit: '_account.move.line.invoice',

  metadata: {
    context: {
      default_move_type: 'out_invoice'
    }
  }
}

const _AccountMoveLineInvoice = {
  // 1. invoice 时使用
  _inherit: 'account.move.line',

  metadata: {
    description: '',

    context: {
      default_currency_id: false,
      default_partner_id: false
      // default_type: 'out_invoice'
    },

    fieldsForQuery: {
      date: null,
      // journal_id: null,
      // move_id: null,
      account_id: null,
      partner_id: null,
      // ref: null,
      name: null,
      analytic_account_id: null,
      debit: null,
      credit: null,
      // full_reconcile_id: null

      product_id: null,

      quantity: null,
      price_unit: null,
      price_subtotal: null,
      price_total: null,

      company_id: null,

      currency_id: null,
      date_maturity: null,
      discount: null,
      display_type: null,

      product_uom_id: null,
      purchase_line_id: null

      //   move_name: "/"
      //   parent_state: "draft"

      //   account_internal_type: 'other',
      //   always_set_currency_id: [7, 'CNY'],
      //   amount_currency: 0,
      //   analytic_tag_ids: [[5]],
      //   asset_category_id: false,
      //   company_currency_id: [7, 'CNY'],
      //   exclude_from_invoice_tab: false,
      //   is_rounding_line: false,
      //   recompute_tax_line: false,
      //   sequence: 10,
      //   tag_ids: [[5]],
      //   tax_base_amount: 0,
      //   tax_exigible: true,
      //   tax_ids: [[5]],
      //   tax_line_id: false,
      //   tax_repartition_line_id: false
    },

    forAppList: {
      title: rec => {
        return `${rec.name}`
      },
      icon: rec => {
        // 编辑的 新增的时候 没有 product_id__object 字段
        return `${(rec.product_id__object || {}).image_128__url}`
      },

      // label: rec => {
      //   return `${rec.email || ''}`
      // },

      value: rec => {
        return `¥${rec.price_subtotal} = ${rec.quantity} * ¥${rec.price_unit}`
      }
    },

    columnsForList: {
      product_id: null,
      name: null,
      account_id: null,
      analytic_account_id: null,
      quantity: null,
      price_unit: null,
      price_subtotal: null
    },

    columnsForForm: {
      product_id: null,
      name: null,
      account_id: null,
      analytic_account_id: null,
      quantity: null,
      price_unit: null,
      price_subtotal: null
    }
  },

  extend: BaseClass => {
    class ModelClass extends BaseClass {
      async onchange(payload = {}) {
        // console.log('onchange,', this.model, payload)
        const { values = {}, context: context_src = {} } = payload
        const { move_id = {} } = values
        const { journal_id } = move_id
        const context = { ...context_src, journal_id }
        return super.onchange({ ...payload, context })
      }
    }
    return ModelClass
  }
}

const AccountMoveLineInvoiceIn13 = {
  _inherit: '_account.move.line.invoice.in',

  metadata: {
    description: '',
    context: {
      default_type: 'in_invoice'
    }
  }
}

const AccountMoveLineInvoiceIn14 = {
  _inherit: '_account.move.line.invoice.in',

  metadata: {
    description: '',
    context: {
      default_move_type: 'in_invoice'
    }
  }
}

const _AccountMoveLineInvoiceIn = {
  // 1. invoice 时使用
  _inherit: 'account.move.line.invoice',

  metadata: {
    description: '',
    context: {
      // default_type: 'in_invoice'
    },

    columnsForForm: {
      product_id: null,
      quantity: null,
      price_unit: null,
      account_id: null,
      analytic_account_id: null,
      name: null
      // price_subtotal: null
    }
  }
}

const AccountMoveInvoice = {
  _inherit: 'account.move',

  metadata: {
    _isMenu: 1, // 控制菜单用的,
    description: '',

    btnNew: { hidden: false, label: '新增' },
    btnView: { hidden: true, label: '查看' },
    btnEdit: { hidden: rec => rec.state !== 'draft', label: '编辑' },
    btnDel: { hidden: rec => rec.state === 'posted', label: '删除' },

    fieldsForQuery: {
      invoice_date: null,
      invoice_partner_display_name: null,
      invoice_origin: null,
      invoice_user_id: null,
      invoice_date_due: null,
      amount_residual_signed: null,
      invoice_payment_state: null,
      invoice_incoterm_id: null,
      invoice_sent: null,

      invoice_line_ids: null,
      invoice_filter_type_domain: null,
      invoice_payment_ref: null,
      invoice_payments_widget: null,
      invoice_sequence_number_next: null,
      invoice_sequence_number_next_prefix: null
    },

    fieldsForBrowse: {
      currency_id: null,
      amount_total: null,
      amount_residual: null,
      invoice_has_outstanding: null,
      invoice_outstanding_credits_debits_widget: null,
      invoice_payments_widget: null,
      invoice_line_ids: {}
    },

    context: {
      // 销售账单 和 采购账单 分别 设置
      // default_type: 'out_invoice'
      // default_type: 'in_invoice'
    },

    workflow: {
      state: rec => {
        return rec.state === 'posted' && rec.invoice_payment_state === 'paid'
          ? 'paid'
          : rec.state
      },
      steps: {
        draft: { string: '草稿' },
        posted: { string: '确认' },
        paid: { string: '收款', hidden: rec => rec.state === 'cancel' },
        cancel: { string: '取消', hidden: rec => rec.state !== 'cancel' } // 仅在 取消后显示
      },

      buttons: {
        action_post: {
          type: 'object',
          string: '确认',
          message: '确认票据?',
          btn_type: 'primary',
          hidden: rec => rec.state !== 'draft'
        },

        // 从账单 到 收款单, 直接去创建  account.paymemt, 不再走这个 wizard 了
        // register_payment: {
        //   type: 'action',
        //   string: '登记收款',
        //   message: '确认要登记收款?',
        //   btn_type: 'primary',
        //   hidden: rec =>
        //     rec.invoice_payment_state !== 'not_paid' || rec.state !== 'posted',

        //   wizard: {
        //     submit: true,
        //     model: 'account.payment.register',
        //     vals: {},
        //     context: {},
        //     fields: {},
        //     method: 'create_payments'
        //   }
        // },

        action_invoice_register_payment: {
          type: 'object_action',
          string: '登记收款',
          message: '确认要登记收款?',
          btn_type: 'primary',
          hidden: rec =>
            rec.invoice_payment_state !== 'not_paid' || rec.state !== 'posted',
          path: '/form/AccountPaymentCustomerIn'
        },

        button_cancel: {
          type: 'object',
          string: '取消',
          message: '取消票据?',
          hidden: rec => rec.state !== 'draft'
        },
        button_draft: {
          type: 'object',
          string: '重置为草稿',
          message: '重置为草稿?',
          hidden: rec => !['posted', 'cancel'].includes(rec.state)
        }
      }
    },

    columnsForPage: {
      partner_id: { type: 'select', label: '客户' },
      ref: { label: '编号' },
      invoice_date: { type: 'date', label: '开票日期' },
      date: { type: 'date', label: '会计日期' },
      invoice_user_id: { type: 'select', label: '销售员' },

      name: { label: '号码' },
      invoice_partner_display_name: { label: '客户' },
      invoice_origin: { label: '源单据' },

      invoice_date_due: { type: 'date', label: '到期日期' },
      amount_total_signed: { type: 'monetary', label: '合计' },
      amount_residual_signed: { type: 'monetary', label: '到期金额' },

      amount_total: { type: 'monetary', label: '合计' },

      amount_residual: { type: 'monetary', label: '到期金额' },

      // state: { type: 'select', label: '状态' },
      invoice_payment_state: { type: 'select', label: '收款状态' },
      // date: { type: 'date', label: '会计日期' },

      // invoice_has_outstanding: { label: '有可核销的收款单' },
      // invoice_outstanding_credits_debits_widget: { label: '待收款单' },
      // invoice_payments_widget: { label: '收款单' },

      invoice_line_ids: {
        type: 'table',
        label: '明细',
        vuxModel: 'accountMoveLineInvoice', // viewO2m, formO2m 需要这个去找模型
        odooModel: 'account.move.line.invoice', //
        // relation_field
        refName: 'move_id' // // viewO2m, formO2m 需要这个补充 onchange 的 values
      },

      invoice_payment__extend: {
        type: 'table',
        data: 'invoice_payment__extend',
        label: '已收款',

        columns: {
          ref: { label: '收款单编号' },
          journal_name: { label: '收款账簿' },
          date: { type: 'date', label: '收款日期' },
          amount: { type: 'monetary', align: 'right', label: '小记' }
        },
        forAppList: {
          title: rec => `${rec.ref}`,
          label: rec => `${rec.date || ''}`,
          value: rec => `${rec.journal_name}: ¥${rec.amount}`
          // icon: rec => {
          //   return `${rec.image_128__url}`
          //   // get_image('res.partner', rec.id, 'image_1920')
          // },
        }
      },

      invoice_outstand__extend: {
        type: 'table',
        data: 'invoice_outstand__extend',
        label: '可核销的收款单',
        hidden: rec => !rec.invoice_has_outstanding,

        columns: {
          actionBefore: {
            type: 'action',
            width: 200,
            buttons: {
              js_assign_outstanding_line: {
                icon: 'ios-add',
                label: '核销收款单',
                message: '核销收款单?'
              }
            }
          },
          journal_name: { label: '收款单编号' },
          payment_date: { type: 'date', label: '收款日期' },
          amount: { type: 'monetary', align: 'right', label: '小记' }
        },
        forAppList: {
          action: {
            method: 'js_assign_outstanding_line',
            icon: 'plus',
            label: '核销',
            message: '核销收款单?'
          },
          title: rec => `${rec.journal_name}`,
          label: rec => `${rec.payment_date || ''}`,
          value: rec => `¥${rec.amount}`
        }
      },

      line_ids: {
        type: 'table',
        odooModel: 'account.move.line.for.invoice', //
        vuxModel: 'accountMoveLineForInvoice', // viewO2m, formO2m 需要这个去找模型
        refName: 'move_id', // // viewO2m, formO2m 需要这个补充 onchange 的 values
        label: '会计分录'
      }
    }
  },

  extend: BaseClass => {
    class ModelClass extends BaseClass {
      get_record(res) {
        const res2 = super.get_record(res)
        // const res3 = { ...res2 }
        const out1 = res.invoice_outstanding_credits_debits_widget
        const out2 = out1 ? JSON.parse(out1) : {}
        const outstand = out2.content || []

        const pay1 = res.invoice_payments_widget
        const pay2 = pay1 ? JSON.parse(pay1) : {}
        const payment = pay2.content || []

        return {
          ...res2,
          invoice_outstand__extend: outstand,
          invoice_payment__extend: payment
        }
      }

      async js_assign_outstanding_line(rid, { line_values: { id: line_id } }) {
        return await this.call('js_assign_outstanding_line', [rid, line_id])
      }
    }
    return ModelClass
  }
}

const _AccountMoveInvoiceOut = {
  _inherit: 'account.move.invoice',

  metadata: {
    description: '',

    context: {
      // default_type: 'out_invoice'
    },

    columnsForList: {
      name: null,
      invoice_partner_display_name: null,
      invoice_date: null,
      invoice_origin: null,
      invoice_user_id: null,
      invoice_date_due: null,
      amount_total_signed: null,
      amount_residual_signed: null,
      state: null,
      invoice_payment_state: null
    },

    columnsForView: {
      name: null,
      partner_id: null,
      ref: null,
      invoice_date: null,
      invoice_date_due: null,
      invoice_user_id: null,
      amount_total: null,
      amount_residual: null,
      invoice_line_ids: {},

      invoice_payment_state: null,
      invoice_payment__extend: {},
      invoice_outstand__extend: {},
      line_ids: {}
    },
    columnsForForm: {
      partner_id: null,
      ref: null,
      invoice_date: null,
      invoice_date_due: null,
      // invoice_user_id: null,
      invoice_line_ids: {}
    }
  },
  extend: BaseClass => {
    class ModelClass extends BaseClass {}
    return ModelClass
  }
}

const AccountMoveInvoiceOut13 = {
  _inherit: '_account.move.invoice.out',

  metadata: {
    _isMenu: 1, // 控制菜单用的,
    context: {
      default_type: 'out_invoice'
    }
  },

  extend: BaseClass => {
    class ModelClass extends BaseClass {
      global_domain(payload = {}) {
        const { domain = {} } = payload
        return super.global_domain({
          ...payload,
          domain: { ...domain, type: 'out_invoice' }
        })
      }
    }
    return ModelClass
  }
}

const AccountMoveInvoiceOut14 = {
  _inherit: '_account.move.invoice.out',

  metadata: {
    _isMenu: 1, // 控制菜单用的,
    context: {
      default_move_type: 'out_invoice'
    }
  },

  extend: BaseClass => {
    class ModelClass extends BaseClass {
      global_domain(payload = {}) {
        const { domain = {} } = payload
        return super.global_domain({
          ...payload,
          domain: { ...domain, move_type: 'out_invoice' }
        })
      }
    }
    return ModelClass
  }
}

const _AccountMoveInvoiceIn = {
  _inherit: 'account.move.invoice',

  metadata: {
    description: '',

    context: {
      // default_type: 'in_invoice'
      // default_move_type: 'in_invoice'
    },

    fieldsForQuery: {
      purchase_id: null
    },

    workflow: {
      steps: { paid: { string: '付款' } },

      buttons: {
        action_post: { message: '确认账单?' },
        // register_payment: { string: '登记付款', message: '确认要登记付款?' },

        action_invoice_register_payment: {
          type: 'object_action',
          string: '登记付款',
          message: '确认要登记付款?',
          // btn_type: 'primary',
          // hidden: rec =>
          //   rec.invoice_payment_state !== 'not_paid' || rec.state !== 'posted',
          path: '/form/AccountPaymentSupplierOut'
        },

        button_cancel: { message: '取消账单?' }
      }
    },

    columnsForPage: {
      partner_id: { type: 'select', label: '供应商' },

      invoice_partner_display_name: { label: '供应商' },
      invoice_date: { type: 'date', label: '账单日期' },

      invoice_user_id: { type: 'select', label: '采购员' },

      // invoice_payment_state: { type: 'select', label: '付款状态' },

      invoice_line_ids: {
        odooModel: 'account.move.line.invoice.in', //
        vuxModel: 'accountMoveLineInvoiceIn' // viewO2m, formO2m 需要这个去找模型
      },

      invoice_payment__extend: {
        // type: 'table',
        // data: 'invoice_payment__extend',
        label: '已付款',
        columns: {
          ref: { label: '付款单编号' },
          journal_name: { label: '付款账簿' },
          date: { type: 'date', label: '付款日期' }
          // amount: { type: 'monetary', align: 'right', label: '小记' }
        }
      },
      invoice_outstand__extend: {
        // type: 'table',
        // data: 'invoice_outstand__extend',
        label: '可核销的付款单',
        columns: {
          actionBefore: {
            buttons: {
              add: {
                label: '核销付款单'
              }
            }
          },
          journal_name: { label: '付款单编号' },
          payment_date: { type: 'date', label: '付款日期' }
        }
      }
    },

    columnsForList: {
      name: null,
      invoice_partner_display_name: null,
      invoice_date: null,
      invoice_origin: null,
      invoice_user_id: null,
      invoice_date_due: null,
      amount_total_signed: null,
      amount_residual_signed: null,
      state: null
      // invoice_payment_state: null
    },

    columnsForView: {
      name: null,
      partner_id: null,
      ref: null,
      invoice_date: null,
      date: null,
      invoice_date_due: null,
      // invoice_user_id: null,
      amount_total: null,
      invoice_line_ids: {},
      // invoice_origin: null,
      invoice_payment_state: null,
      invoice_payment__extend: {},
      invoice_outstand__extend: {},
      line_ids: {}
    },

    columnsForForm: {
      partner_id: null,
      ref: null,
      invoice_date: null,
      date: null,
      invoice_date_due: null,
      invoice_user_id: null,
      invoice_line_ids: {}

      // invoice_origin: null,
    }
  },

  extend: BaseClass => {
    class ModelClass extends BaseClass {
      global_domain(payload = {}) {
        // const { domain = {} } = payload
        return super.global_domain({
          ...payload
          // domain: { ...domain, move_type: 'in_invoice' }
          // domain: { ...domain, type: 'in_invoice' }
        })
      }
    }
    return ModelClass
  }
}

const AccountMoveInvoiceIn13 = {
  _inherit: '_account.move.invoice.in',

  metadata: {
    _isMenu: 1, // 控制菜单用的,
    context: {
      default_type: 'in_invoice'
    }
  },

  extend: BaseClass => {
    class ModelClass extends BaseClass {
      global_domain(payload = {}) {
        const { domain = {} } = payload
        return super.global_domain({
          ...payload,
          domain: { ...domain, type: 'in_invoice' }
        })
      }
    }
    return ModelClass
  }
}

const AccountMoveInvoiceIn14 = {
  _inherit: '_account.move.invoice.in',

  metadata: {
    _isMenu: 1, // 控制菜单用的,
    context: {
      default_move_type: 'in_invoice'
    }
  },

  extend: BaseClass => {
    class ModelClass extends BaseClass {
      global_domain(payload = {}) {
        const { domain = {} } = payload
        return super.global_domain({
          ...payload,
          domain: { ...domain, move_type: 'in_invoice' }
        })
      }
    }
    return ModelClass
  }
}

const odooVersion = process.env.VUE_APP_ODOO_VERSION
console.log('version', odooVersion, typeof odooVersion)

const AccountMoveInvoiceOut =
  odooVersion === '14' ? AccountMoveInvoiceOut14 : AccountMoveInvoiceOut13

const AccountMoveInvoiceIn =
  odooVersion === '14' ? AccountMoveInvoiceIn14 : AccountMoveInvoiceIn13

const AccountMoveLineInvoice =
  odooVersion === '14' ? AccountMoveLineInvoice14 : AccountMoveLineInvoice13
const AccountMoveLineInvoiceIn =
  odooVersion === '14' ? AccountMoveLineInvoiceIn14 : AccountMoveLineInvoiceIn13

const Models = {
  'account.move.invoice': AccountMoveInvoice,

  '_account.move.invoice.out': _AccountMoveInvoiceOut,
  'account.move.invoice.out': AccountMoveInvoiceOut,
  '_account.move.invoice.in': _AccountMoveInvoiceIn,
  'account.move.invoice.in': AccountMoveInvoiceIn,

  'account.move.line.for.invoice': AccountMoveLineForInvoice,

  '_account.move.line.invoice': _AccountMoveLineInvoice,
  'account.move.line.invoice': AccountMoveLineInvoice,

  '_account.move.line.invoice.in': _AccountMoveLineInvoiceIn,
  'account.move.line.invoice.in': AccountMoveLineInvoiceIn
}

export default Models

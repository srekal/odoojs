const AccountJournal = {
  metadata: {
    _isMenu: 0, // 控制菜单用的,
    description: '',
    fields: {},
    fieldsForQuery: {
      name: null,
      type: null
    },

    btnNew: { hidden: true, label: '新增' },
    btnView: { hidden: true, label: '查看' },
    btnEdit: { hidden: true, label: '编辑' },
    btnDel: { hidden: true, label: '删除' },

    columnsForList: {
      name: { label: '名称' },
      type: { type: 'select', label: '类型' }
    }
  }
}

const AccountAccount = {
  metadata: {
    _isMenu: 1, // 控制菜单用的,
    description: '',

    fields: {},

    fieldsForQuery: {
      name: null,
      code: null,
      user_type_id: null,
      company_id: null
    },

    btnNew: { hidden: true, label: '新增' },
    btnView: { hidden: true, label: '查看' },
    btnEdit: { hidden: true, label: '编辑' },
    btnDel: { hidden: true, label: '删除' },

    // 移动端用,
    forAppList: {
      title: rec => {
        return `${rec.display_name}`
      },
      // icon: rec => {
      //   return `${rec.image_128__url}`
      //   // get_image('res.partner', rec.id, 'image_1920')
      // },

      label: rec => {
        return `${rec.user_type_id__name || ''}`
      }
      // value: rec => {
      //   return `余额: ${rec.company_id__name || ''}`
      // }
    },

    workflow: {
      buttons: {
        search_invoice: {
          type: 'router',
          string: '查看会计分录',
          message: '查看会计分录?',
          path: '/list/AccountMoveLinePosted',
          domain(rec) {
            return {
              account_id: rec.id
            }
          }
        }
      }
    },

    columnsForFilter: {
      // code___like: { label: '代码', placeholder: '代码' },
      // name___like: { label: '科目', placeholder: '科目名称' },

      name_or_code: { label: '科目', placeholder: '科目代码或名称' }

      // user_type_id: {
      //   type: 'select',
      //   domain: [],
      //   label: '类型',
      //   placeholder: '类型'
      // }
    },

    columnsForPage: {
      // id: { label: 'ID' },
      code: { label: '代码' },
      name: { label: '名称' },
      user_type_id: { type: 'select', label: '类型' }
    },

    columnsForList: {
      // id: { label: 'ID' },
      code: null,
      name: null,
      user_type_id: null
    },
    columnsForView: {
      // id: { label: 'ID' },
      code: null,
      name: null,
      user_type_id: null
    }
  },

  extend: BaseClass => {
    class ModelClass extends BaseClass {
      async web_search_read(query = {}) {
        // console.log('web_search_read', query)
        const { domain: domain1 = {} } = query
        const { name_or_code, ...domain } = domain1

        const domain2 = name_or_code
          ? [
              '|',
              ['name', 'like', name_or_code],
              ['code', 'like', name_or_code]
            ]
          : []

        return super.web_search_read({ ...query, domain2, domain })
      }
    }
    return ModelClass
  }
}

const AccountAnalyticAccount = {
  metadata: {
    _isMenu: 0, // 控制菜单用的,
    description: '',

    fields: {},

    //
    fieldsForQuery: {
      group_id: null,
      name: null,
      code: null,
      partner_id: null,
      debit: null,
      credit: null,
      balance: null
    },

    columnsForList: {
      group_id__name: { label: '分组' },
      name: { label: '名称' },
      code: { label: '编号' },
      partner_id__name: { label: '业务伙伴' },
      debit: { label: '借方' },
      credit: { label: '贷方' },
      balance: { label: '余额' }
    }
  }
}

const Models = {
  'account.journal': AccountJournal,
  'account.account': AccountAccount,
  'account.analytic.account': AccountAnalyticAccount
}

export default Models

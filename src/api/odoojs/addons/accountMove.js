const AccountMoveLine = {
  metadata: {
    description: '',

    fields: {
      product_id: { type: 'many2one', relation: 'product.product' }
    },

    field_onchange_ref: 'account.view_move_form',
    field_onchange_model: 'account.move',
    field_onchange_field: 'line_ids',

    btnNew: { hidden: true, label: '新增' },
    btnView: { hidden: true, label: '查看' },
    btnEdit: { hidden: true, label: '编辑' },
    btnDel: { hidden: true, label: '删除' },

    forAppList: {
      title: rec => {
        return `${rec.account_id__name}`
      },
      // icon: rec => {
      //   // 编辑的 新增的时候 没有 product_id__object 字段
      //   return `${(rec.product_id__object || {}).image_128__url}`
      // },

      label: rec => {
        return `${rec.name || ''}`
      },

      value: rec => {
        return `借:${rec.debit}  贷: ${rec.credit}`
      }
    },

    fieldsForQuery: {
      // payment_id: null,
      date: null,
      journal_id: null,
      move_id: null,
      account_id: null,
      partner_id: null,
      ref: null,
      name: null,
      analytic_account_id: null,
      debit: null,
      credit: null,
      full_reconcile_id: null,

      // credit: 0,
      // debit: 0,
      // partner_id: false,
      currency_id: false,
      discount: 0,
      display_type: false,
      quantity: 1,
      sequence: 10,
      tax_exigible: true
    },

    fieldsForBrowse22: {
      account_id: '1',
      partner_id: '',
      name: '',
      analytic_account_id: '1',
      currency_id: '1',
      debit: '1',
      credit: '1',
      date_maturity: '',
      amount_currency: '1',
      product_id: '1',
      quantity: '1',
      product_uom_id: '',
      price_unit: '1',
      discount: '1',
      price_subtotal: '1',
      price_total: '',
      sequence: '',
      move_name: '',
      date: '',
      company_id: '1',
      company_currency_id: '',

      asset_category_id: '1',
      analytic_tag_ids: '1',
      tax_ids: '1',
      country_id: '',
      tag_ids: '1',
      payment_id: '1',
      tax_line_id: '1',
      tax_group_id: '',
      tax_repartition_line_id: '1',
      tax_base_amount: '1',
      tax_exigible: '',
      purchase_line_id: '',
      recompute_tax_line: '',
      display_type: '',
      is_rounding_line: '',
      exclude_from_invoice_tab: '1',
      account_internal_type: ''
    },

    context: {},

    columnsForFilter: {
      account_id: { type: 'select', label: '科目', placeholder: '科目' }
    },

    columnsForPage: {
      date: { type: 'date', label: '日期' },
      journal_id: { type: 'select', label: '日记账' },
      move_id: { type: 'select', label: '凭证' },
      // 做级联? 选项太多了
      account_id: { type: 'select', label: '科目' },
      partner_id: { type: 'select', label: '业务伙伴' },
      ref: { label: '编号' },
      name: { label: '标签' },
      analytic_account_id: { type: 'select', label: '核算项目' },
      debit: { type: 'monetary', label: '借方' },
      credit: { type: 'monetary', label: '贷方' },
      full_reconcile_id: { type: 'select', label: '核销' },

      product_id: { type: 'select', label: '产品' },
      quantity: { type: 'number', align: 'right', label: '数量' },
      price_unit: { type: 'monetary', align: 'right', label: '单价' },
      price_subtotal: {
        readonly: true,
        type: 'monetary',
        align: 'right',
        label: '小记'
      }

      // payment_id: { label: 'payment_id' },
      // id: { label: 'ID' },
    },

    columnsForList: {
      date: null,
      // journal_id: null,
      // move_id: null,
      account_id: null,
      partner_id: null,
      ref: null,
      name: null,
      // analytic_account_id: null,
      debit: null,
      credit: null
      // full_reconcile_id: null

      // payment_id: null,
      // id: null,
    },

    columnsForView: {
      date: null,
      journal_id: null,
      move_id: null,
      account_id: null,
      partner_id: null,
      ref: null,
      name: null,
      analytic_account_id: null,
      debit: null,
      credit: null,
      full_reconcile_id: null
    }
  }
}

const AccountMoveLinePosted = {
  // 直接查询会计分录时使用
  _inherit: 'account.move.line',
  metadata: {
    _isMenu: 1 // 控制菜单用的,
  },
  extend: BaseClass => {
    class ModelClass extends BaseClass {
      async global_domain(payload = {}) {
        const { domain = {} } = payload
        return super.global_domain({
          ...payload,
          domain: { ...domain, 'move_id.state': 'posted' }
        })
      }
    }
    return ModelClass
  }
}

const AccountMoveLineEntry13 = {
  _inherit: '_account.move.line.entry',
  metadata: {
    context: {
      default_type: 'entry'
    }
  }
}
const AccountMoveLineEntry14 = {
  _inherit: '_account.move.line.entry',
  metadata: {
    context: {
      default_move_type: 'entry'
    }
  }
}

const _AccountMoveLineEntry = {
  // 1. 录入杂项凭证时使用
  // 2. 录入 期初数 时使用
  _inherit: 'account.move.line',

  metadata: {
    description: '',
    context: {
      default_currency_id: false,
      default_partner_id: false,
      // default_type: 'entry',
      view_no_maturity: true
    },

    columnsForList: {
      account_id: null,
      partner_id: null,
      name: null,
      debit: null,
      credit: null
    },

    columnsForForm: {
      account_id: null,
      partner_id: null,
      name: null,
      debit: null,
      credit: null
    }
  },

  extend: BaseClass => {
    class ModelClass extends BaseClass {
      async onchange(payload = {}) {
        // console.log('onchange,', this.model, payload)
        const { values = {}, context: context_src = {} } = payload
        const { move_id = {} } = values
        const { journal_id, line_ids = [] } = move_id
        const context = { ...context_src, journal_id, line_ids }
        return super.onchange({ ...payload, context })
      }
    }
    return ModelClass
  }
}

const _AccountMove = {
  // 按照 type 分类
  // 从 invoice / payment 自动过来的 凭证, 只读
  // 1 out_invoice / out_refund 客户发票 红发票
  // 2 in_invoice / in_refund 供应商账单 红账单
  // 3 out_receipt / in_receipt 付款单 收款单
  // 4 entry 杂项
  // type===entry 的凭证, 可以创建 编辑

  // 所有分录, 包括 销售/采购账单/收/付款/杂项(期初/库存/生产成本)
  // 只读

  _name: 'account.move',

  metadata: {
    _isMenu: 0, // 控制菜单用的,
    description: '',

    fields: {},
    field_onchange_ref: 'account.view_move_form',
    // field_onchange_model: 'account.move',
    // field_onchange_field: '',

    btnNew: { hidden: true, label: '新增' },
    btnView: { hidden: true, label: '查看' },
    btnEdit: { hidden: true, label: '编辑' },
    btnDel: { hidden: true, label: '删除' },

    // account.move 不需要 default_company_id 了么?
    // TBD check
    // context: {
    //   default_company_id: self => {
    //     const company_id = self.get_company_id()
    //     return company_id
    //   }
    // },

    forAppList: {
      title: rec => {
        return `${rec.display_name}`
      },
      // icon: rec => {
      //   return `${rec.image_128__url}`
      //   // get_image('res.partner', rec.id, 'image_1920')
      // },

      label: rec => {
        return `${rec.state__name || ''}`
      },
      value: rec => {
        return `¥${rec.amount_total_signed}`
      }
    },

    fieldsForQuery: {
      // type: null, // version 13
      // move_type: null, // version 14
      ref: null,
      date: null,
      name: null,
      partner_id: null,
      journal_id: null,
      amount_total_signed: null,
      state: null,
      line_ids: {},

      auto_post: null,
      company_id: null,
      currency_id: null,
      team_id: null,
      to_check: null,
      type: null,

      amount_by_group: null,
      amount_residual: null,
      amount_total: null,
      amount_untaxed: null,
      bank_partner_id: null,
      commercial_partner_id: null,
      company_currency_id: null,

      partner_shipping_id: null
    },

    columnsForFilter: {
      state: { type: 'select', label: '状态', placeholder: '状态' }
    },

    columnsForPage: {
      ref: { label: '编号' },
      date: { type: 'date', label: '日期' },
      name: { label: '名称' },
      state: { type: 'select', label: '状态' },
      amount_total_signed: { type: 'monetary', label: '合计' },
      // journal_id: { type: 'select', label: '凭证类型' },
      // id: { label: 'ID' },
      // type: { type: 'select', label: '类型' },

      line_ids: {
        type: 'table',
        odooModel: 'account.move.line', //
        vuxModel: 'accountMoveLine', // viewO2m, formO2m 需要这个去找模型
        refName: 'move_id', // // viewO2m, formO2m 需要这个补充 onchange 的 values
        label: '会计分录'
      }
    },

    columnsForList: {
      date: null,
      name: null,
      ref: null,
      amount_total_signed: null,
      state: null
    },

    columnsForView: {
      date: null,
      name: null,
      ref: null,
      amount_total_signed: null,
      state: null,
      line_ids: {}

      // partner_id: null,
    }
  },

  extend: BaseClass => {
    class ModelClass extends BaseClass {}
    return ModelClass
  }
}

const AccountMove13 = {
  // _name: 'account.move',
  _inherit: '_account.move',

  metadata: {
    _isMenu: 1, // 控制菜单用的,
    fieldsForQuery: {
      type: null
    }
  }
}

const AccountMove14 = {
  // _name: 'account.move',
  _inherit: '_account.move',

  metadata: {
    _isMenu: 1, // 控制菜单用的,
    fieldsForQuery: {
      move_type: null
    }
  }
}

const _AccountMoveEntry = {
  _inherit: 'account.move',

  // 杂项分录  // 包括期初数, 库存核算
  // 期初分录 被删除了， 好像也没事
  // 其实 可以直接在这里做期初数的
  // 库存核算分录, 有产品和数量, 应该单独做个模型

  // 草稿 确认 取消
  // 草稿状态 可以编辑
  // 确认后不可编辑
  // 可以取消后, 再重置为草稿, 重新编辑
  // 可以删除

  metadata: {
    description: '',
    btnNew: { hidden: false, label: '新增' },
    btnView: { hidden: true, label: '查看' },
    btnEdit: { hidden: rec => rec.state !== 'draft', label: '编辑' },
    btnDel: { hidden: rec => rec.state === 'posted', label: '删除' },

    context: {
      // default_type: 'entry', // version 13
      // default_move_type: 'entry', // version 14
      view_no_maturity: true
    },

    workflow: {
      // state: rec => rec.state,
      steps: {
        draft: { string: '草稿' },
        posted: { string: '确认' },
        cancel: { string: '取消', hidden: rec => rec.state !== 'cancel' } // 仅在 取消后显示
      },

      buttons: {
        action_post: {
          type: 'object',
          string: '确认',
          message: '确认凭证?',
          btn_type: 'primary',
          hidden: rec => rec.state !== 'draft'
        },

        button_cancel: {
          type: 'object',
          string: '取消',
          message: '取消凭证?',
          hidden: rec => rec.state !== 'draft'
        },
        button_draft: {
          type: 'object',
          string: '重置为草稿',
          message: '重置为草稿?',
          hidden: rec => !['posted', 'cancel'].includes(rec.state)
        }
      }
    },

    columnsForPage: {
      line_ids: {
        odooModel: 'account.move.line.entry', //
        vuxModel: 'accountMoveLineEntry' // viewO2m, formO2m 需要这个去找模型
      }
    },

    columnsForList: {
      date: null,
      name: null,
      ref: null,
      amount_total_signed: null,
      state: null
    },

    columnsForView: {
      name: null,
      ref: null,
      date: null,
      amount_total_signed: null,
      state: null,
      line_ids: {}
    },

    columnsForForm: {
      ref: null,
      date: null,
      line_ids: {}
    }
  },

  extend: BaseClass => {
    class ModelClass extends BaseClass {
      global_domain(payload = {}) {
        const { domain = {} } = payload
        return super.global_domain({
          ...payload,
          domain: { ...domain, 'journal_id.type': 'general' }
        })
      }
    }
    return ModelClass
  }
}

const AccountMoveEntry13 = {
  _inherit: '_account.move.entry',

  metadata: {
    _isMenu: 1, // 控制菜单用的,
    context: {
      default_type: 'entry'
    }
  }
}

const AccountMoveEntry14 = {
  _inherit: '_account.move.entry',

  metadata: {
    _isMenu: 1, // 控制菜单用的,
    context: {
      default_move_type: 'entry'
    }
  }
}

const AccountMoveOpening = {
  _inherit: 'account.move.entry',

  // 期初分录, 有且只有一条 account.move
  // 无时可以创建, 有了就不能创建了
  // 草稿状态 可以编辑
  // 确认后不可编辑
  // 可以取消后, 再重置为草稿, 重新编辑
  // 不能删除

  metadata: {
    _isMenu: 1, // 控制菜单用的,
    description: '',
    btnNew: {
      hidden: dateList => dateList && dateList.length,
      label: '录入期初数'
    },
    btnView: { hidden: true, label: '查看' },
    btnEdit: { hidden: rec => rec.state !== 'draft', label: '录入期初数' },
    btnDel: { hidden: true, label: '删除' }
  },

  extend: BaseClass => {
    class ModelClass extends BaseClass {
      async global_domain(payload = {}) {
        // 期初分录, 从 res.company 中 过滤
        const { domain = {} } = payload

        const userinfo = this.get_userinfo()
        // console.log('userinfo', userinfo)
        const { company_id } = userinfo
        const comp = await this.env('res.company').browse_one(company_id, {
          fields: ['account_opening_move_id']
        })

        const move_id = comp.account_opening_move_id || [0, null]

        return super.global_domain({
          ...payload,
          domain: { ...domain, id: move_id[0] }
        })
      }

      async create(values = {}, kwargs = {}) {
        // 创建时 应该 检查 是否已有, 目前通过 页面 新增按钮 显示隐藏, 来控制
        // 期初分录, 创建后, 设置 res.company
        const res = await super.create(values, kwargs)
        const { id: move_id } = res
        const userinfo = this.get_userinfo()
        const { company_id } = userinfo
        await this.env('res.company').write(company_id, {
          account_opening_move_id: move_id
        })

        return res
      }
    }
    return ModelClass
  }
}

const odooVersion = process.env.VUE_APP_ODOO_VERSION
console.log('version', odooVersion, typeof odooVersion)

// odoo 多版本机制
// 不同版本, 有微小差异
// 根据 版本号不同, 做不同的定义
// 公共部分 抽象出一个 父定义

const AccountMove = odooVersion === '14' ? AccountMove14 : AccountMove13
const AccountMoveEntry =
  odooVersion === '14' ? AccountMoveEntry14 : AccountMoveEntry13

const AccountMoveLineEntry =
  odooVersion === '14' ? AccountMoveLineEntry14 : AccountMoveLineEntry13

const Models = {
  '_account.move': _AccountMove,
  'account.move': AccountMove,

  '_account.move.entry': _AccountMoveEntry,
  'account.move.entry': AccountMoveEntry,

  'account.move.opening': AccountMoveOpening,

  'account.move.line': AccountMoveLine,
  'account.move.line.posted': AccountMoveLinePosted,

  '_account.move.line.entry': _AccountMoveLineEntry,
  'account.move.line.entry': AccountMoveLineEntry

  // 'account.analytic.account': AccountAnalyticAccount
}

export default Models

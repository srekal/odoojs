const HrDepartment = {
  // _name: 'res.partner',
  metadata: {
    description: '参与人',

    fields: {},

    //
    fieldsForQuery: {
      name: null,
      manager_id: null,
      parent_id: null
    },

    columnsForList: {
      name: { label: '姓名' },
      manager_id__name: { label: '部门经理' },
      parent_id__name: { label: '上级部门' }
    }
  }
}

const HrJob = {
  // _name: 'res.partner',
  metadata: {
    description: '参与人',

    fields: {},

    //
    fieldsForQuery: {
      name: null,
      department_id: null,
      no_of_employee: null
    },

    columnsForList: {
      name: { label: '姓名' },
      department_id__name: { label: '部门' },
      no_of_employee: { label: '员工数量' }
    }
  }
}

const HrEmployee = {
  // _name: 'res.partner',
  metadata: {
    description: '参与人',

    fields: {
      //   image_1920: { type: 'image' },
      //   image_1024: { type: 'image' },
      //   image_512: { type: 'image' },
      //   image_256: { type: 'image' },
      image_128: { type: 'image' }
      //   date: { type: 'date' },
      //   type: { type: 'selection', string: '地址类型' }
    },

    //
    fieldsForQuery: {
      name: null,
      work_phone: null,
      work_email: null,
      department_id: null,
      job_id: null,
      parent_id: null
    },

    columnsForList: {
      image_128__url: { type: 'img', label: '图片' },
      name: { label: '姓名' },
      work_phone: { label: '办工电话' },
      work_email: { label: 'email' },
      department_id__name: { label: '部门' },
      job_id__name: { label: '职位' },
      parent_id__name: { label: '经理' }
    }
  }
}

const HrExpense = {
  metadata: {
    description: '',

    fields: {},

    //
    fieldsForQuery: {
      date: null,
      name: null,
      employee_id: null,
      analytic_account_id: null,
      total_amount: null,
      state: null
    },

    columnsForList: {
      date: { type: 'date', label: '日期' },
      name: { label: '说明' },
      employee_id__name: { label: '员工' },
      analytic_account_id__name: { label: '核算项目' },
      total_amount: { type: 'monetary', label: '合计' },
      state__name: { label: '状态' }
    }
  }
}

const HrExpenseSheet = {
  metadata: {
    description: '',

    fields: {},

    //
    fieldsForQuery: {
      name: null,
      employee_id: null,
      create_date: null,
      accounting_date: null,
      total_amount: null,
      state: null
    },

    columnsForList: {
      name: { label: '费用报表' },
      create_date: { type: 'date', label: '提交日期' },
      accounting_date: { type: 'date', label: '记账日期' },
      employee_id__name: { label: '员工' },
      total_amount: { type: 'monetary', label: '合计' },
      state__name: { label: '状态' }
    }
  }
}

const Models = {
  'hr.department': HrDepartment,
  'hr.job': HrJob,
  'hr.employee': HrEmployee,
  'hr.expense': HrExpense,
  'hr.expense.sheet': HrExpenseSheet
}

export default Models

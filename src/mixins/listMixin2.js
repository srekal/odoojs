import api from '@/api'

function toPrice(num) {
  num = parseFloat(num)
    .toFixed(2)
    .toString()
    .split('.')
  num[0] = num[0].replace(new RegExp('(\\d)(?=(\\d{3})+$)', 'ig'), '$1,')
  return '￥' + num.join('.')
}

export default {
  data() {
    return {
      titleForListPage: '',
      // 控制列表页面的 分页
      domain: {},
      pageSize: 10,
      currentPage: 1,
      pageSizePpts: [200, 2, 4, 6, 8, 10, 12],

      modelName: undefined,
      metadata: {},
      dataList: [],

      // for pc
      dataCount: 0,

      // for app
      dataListAll: [],
      fieldsGet: {},

      // app 上拉加载更多, 下拉刷新
      loading: false,
      finished: false,
      refreshing: false
    }
  },
  computed: {
    // 控制 新增按钮,  显示文本 / 是否隐藏
    btnNew() {
      const btn = this.metadata.btnNew || {}
      btn.label = btn.label || '新增'
      return btn
    },

    // !(btnNew.hidden && typeof btnNew.hidden === 'function'
    // ? btnNew.hidden(dataList)
    // : btnNew.hidden)

    hasBtnNew() {
      const btnHidden = this.btnNew.hidden
      return !(btnHidden && typeof btnHidden === 'function'
        ? btnHidden(this.dataList)
        : btnHidden)
    },

    // 控制 删除按钮,  显示文本 / 是否隐藏
    btnDel() {
      const btn = this.metadata.btnDel || {}
      btn.label = btn.label || '删除'
      return btn
    },

    hasBtnDel() {
      const btnHidden = this.btnDel.hidden
      return !(btnHidden && typeof btnHidden === 'function'
        ? btnHidden(this.dataDict)
        : btnHidden)
    },

    // listPage 用的
    // 移动端 app list 的 title / label / icon / value
    forAppList() {
      const meta = this.metadata.forAppList || {}
      return {
        title: rec => rec.display_name || rec.name || '',
        value: () => '',
        label: () => '',
        icon: () => '',
        ...meta
      }
    },

    // 过滤框
    columnsForFilter() {
      return this.metadata.columnsForFilter || {}
    },

    // pc 控制列表页面的表格
    tabelForList() {
      const tableForList = this.metadata.tableForList || {}
      return {
        border: tableForList.border
      }
    },

    // pc 自动展示列表页面时, 需要的字段
    columnsForList() {
      return this.metadata.columnsForList
    },

    // pc 自动展示列表页面时, 需要的字段
    columnsForList2() {
      const cols = this.metadata.columnsForList
      const cols2 = this.columnsForTable(cols)

      return [
        // { title: 'ID', key: 'id' },
        // { type: 'index', width: 60, align: 'center' },
        ...cols2
        // {
        //   title: 'Action',
        //   slot: 'action',
        //   width: 200,
        //   align: 'center',
        //   fixed: 'right'
        // }
      ]
    },

    downloadHref() {
      // console.log('metadata', this.metadata)
      const baseUrl = process.env.VUE_APP_BASE_API

      const filePath =
        this.metadata.importTemplatePath ||
        '/base_import_patch/static/xls/res_partner.csv'

      return `${baseUrl}${filePath}`
    }
  },

  async created() {},

  methods: {
    columnsForTable(cols) {
      // console.log('columnsForTable, ', cols)
      // .filter(item => cols[item].type !== 'action')
      const cols2 = Object.keys(cols || {}).map(item => {
        const meta = cols[item]
        const item2 = {
          title: cols[item].label,
          key: item,
          width: cols[item].width,
          align: cols[item].align,
          tooltip: cols[item].tooltip
        }

        if (['select', 'radio'].includes(meta.type)) {
          item2.key = `${item}__name`
        }

        if (cols[item].type === 'img') {
          item2.render = (h, params) => {
            // console.log('table, img', h, params)
            return h('img', {
              /*  组件样式 */
              style: {
                width: '60%',
                height: '60%',
                'border-radius': '5%'
              },
              /*  html属性 */
              attrs: {
                /*  图片的路径,直接采用后台返回的键值 */
                src: params.row[`${item}__url`]
              }
            })
          }
        }

        if (cols[item].type === 'monetary') {
          item2.align = 'right'
          item2.render = (h, params) => {
            const val = params.row[params.column.key]
            const val2 = toPrice(val)
            return h('span', {}, val2)
          }
        }

        // if (cols[item].render) {
        //   item2.render = cols[item].render
        // }

        // if (cols[item].type === 'date') {
        //   // item2.slot = cols[item].slot || 'time'
        //   item2.render = (h, params) => {
        //     const time2 = params.row[params.column.key]
        //     const time3 = time2 && time2.slice(0, 10)
        //     return h('span', {}, time3)
        //   }
        // }

        return item2
      })

      // console.log('columnsForList2, isForm, cols', cols)

      return cols2
    },

    init() {
      console.log('1')
      const moduleName = this.$route.meta.name
      this.moduleName = moduleName
      this.modelName = this.$route.meta.model

      this.metadata = { ...api.env(this.modelName).metadata }

      const query = this.$route.query || {}
      const domain = query.domain
      const domain2 = domain ? JSON.parse(domain) : {}

      this.domain = domain2
      this.titleForListPage = query.title

      this.initFilterOptions()
      console.log('2')
    },

    async handleSearch() {
      const model = this.modelName

      const res = await api.env(model).web_search_read({
        domain: this.domain,
        limit: this.pageSize,
        page: this.currentPage
      })

      console.log('list', res)

      this.dataCount = res.length

      this.dataList = [...res.records]
    },

    // 初始化查询操作, 取总记录数, 复位分页参数
    async handleFlash(payload = {}) {
      // console.log('handleFlash')
      const { domain } = payload
      if (domain) {
        this.domain = domain
      }
      this.currentPage = 1
      this.pageSize = 10
      return this.handleSearch()
    },

    // view Page 组件 页面切换时, 传回参数, 当前页面,  获取数据
    async handleSearch2(page) {
      // console.log('handleSearch2', page)
      this.currentPage = page
      return this.handleSearch()
    },

    // view Page 组件 页面大小变化时, 传回参数, 初始化 刷新数据
    handleFlash2(pagesize) {
      // console.log('handleFlash2', pagesize)
      this.pageSize = pagesize
      this.currentPage = 1
      this.handleSearch()
    },

    // app 加载更多
    async appLoadMore() {
      setTimeout(async () => {
        if (this.refreshing) {
          this.currentPage = 1
          this.dataListAll = []
          this.refreshing = false
        }

        await this.handleSearch()

        this.dataListAll = [...this.dataListAll, ...this.dataList]
        this.loading = false
        this.currentPage = this.currentPage + 1

        if (this.dataList.length < this.pageSize) {
          this.finished = true
        }
      }, 1)
    },

    appRefresh() {
      // 清空列表数据
      this.finished = false

      // 重新加载数据
      // 将 loading 设置为 true，表示处于加载状态
      this.loading = true
      this.appLoadMore()
    },
    appSearch() {
      // this.searchCollapse = ''
      this.currentPage = 1
      this.dataListAll = []
      this.appRefresh()
    },

    HandleRowClickForList(rec) {
      // console.log('HandleRowClickForList ,rec', rec)
      this.onBtnView(rec)
    },

    // 点击查看按钮, 跳转查看页面
    onBtnView(rec) {
      // console.log('onBtnView', rec)

      this.$router.push({
        path: this.$route.meta.paths.view,
        query: { id: rec.id }
      })
    },

    fields_get_callback(fld, ops) {
      const meta = this.fieldsGet[fld] || {}
      const meta2 = { ...meta, selection: ops, _set: 1 }
      this.fieldsGet = { ...this.fieldsGet, [fld]: meta2 }
    },

    // 初始化 下拉搜索框
    async initFilterOptions() {
      const callback = (fld, ops) => this.fields_get_callback(fld, ops)
      const model = this.modelName
      const res = await api.env(model).fields_get({ page: 'list', callback })
      console.log('initFilterOptions,', res)
      this.fieldsGet = { ...res }
    },

    async handleDel(rid) {
      // const res =
      await api.env(this.modelName).unlink(rid)
    },

    // TBD 2020-11-4 导入数据 需要 跳转
    async import_data(payload) {
      const model = this.metadata.model
      const obj = this.api.env(model)
      const res = await obj.import_data({ ...payload })
      return res
    }
  }
}
